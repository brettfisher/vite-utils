# Vite Utils

Utility scripts for Stackline website Vite migration.

## Remove duplicate files

Vite doesn't like it when we have JSX inside of .js or .ts files. It wants them to be named .jsx or .tsx.

Jonny already renamed everything, but when we merge `develop`, it pulls in all the old .js or .ts files.

Many of them will have the same contents, but others will not. This script will remove the .js and .ts files with duplicate content
and print out the ones who have the same name but different content that you will need to manually resolve.

Usage:

```
export SL_WEBSITE_ROOT_DIR=/Users/brettfisher/Desktop/stackline-services/website  # path to where you have cloned website repo
curl -s https://gitlab.com/brettfisher/vite-utils/-/raw/main/renameDuplicates.py | python3
```

This will print out duplicates without deleting them. If you want to delete all files with duplicate content:

```
curl -s https://gitlab.com/brettfisher/vite-utils/-/raw/main/renameDuplicates.py | python3 - --delete
```

If you want to see the help message:

```
curl -s https://gitlab.com/brettfisher/vite-utils/-/raw/main/renameDuplicates.py | python3 - --help
```
