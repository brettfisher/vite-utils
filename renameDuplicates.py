import os
import hashlib
import argparse


# Function to calculate the MD5 hash of a file
def md5_file(file_path):
    md5 = hashlib.md5()
    with open(file_path, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b''):
            md5.update(chunk)
    return md5.hexdigest()


def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--root_dir', type=str, help='Root directory to search for files. Can also be the value of the environment variable SL_WEBSITE_ROOT_DIR')
    parser.add_argument('--delete', action='store_true', help='Delete duplicate files with the same content')
    args = parser.parse_args()

    # Get root_dir value from command line argument or environment variable
    root_dir = args.root_dir or os.environ.get('SL_WEBSITE_ROOT_DIR')
    if not root_dir:
        raise ValueError("Root directory not specified. Make sure to export SL_WEBSITE_ROOT_DIR with the path to the website repo.")

    exclude_dirs = {"node_modules", ".git", "dist", "coverage"}
    valid_exts = {".js", ".ts"}

    duplicates = []
    different_content = []

    # Walk through directory tree
    for root, dirs, files in os.walk(root_dir):
        # Exclude specified directories
        dirs[:] = [d for d in dirs if d not in exclude_dirs]

        # Process each file
        for file in files:
            name, ext = os.path.splitext(file)
            duplicate_file_name = f'{name}{ext}x'  # .jsx or .tsx
            if ext in valid_exts and duplicate_file_name in files:
                full_file_path = os.path.join(root, file)
                # Same content
                if md5_file(full_file_path) == md5_file(os.path.join(root, duplicate_file_name)):
                    duplicates.append(full_file_path)
                else:
                    different_content.append(full_file_path)

    print("Duplicate file contents")
    for f in duplicates:
        print(f + (' -- DELETED' if args.delete else ''))
        if args.delete:
            os.remove(f)
    print("\nDifferent content")
    print('\n'.join(different_content))


if __name__ == "__main__":
    main()
